export const REPOSITORIES_READ_REQUEST = 'REPOSITORIES_READ_REQUEST';
export const REPOSITORIES_READ_SUCCESS = 'REPOSITORIES_READ_SUCCESS';
export const REPOSITORIES_READ_FAILURE = 'REPOSITORIES_READ_FAILURE';

export const REPOSITORIES_THUNK_READ_REQUEST =
  'REPOSITORIES_THUNK_READ_REQUEST';
export const REPOSITORIES_SAGA_READ_REQUEST = 'REPOSITORIES_SAGA_READ_REQUEST';

export function repositoriesReadRequest(playload) {
  return {
    type: REPOSITORIES_READ_REQUEST,
    playload
  };
}

export function repositoriesReadSuccess(playload) {
  return {
    type: REPOSITORIES_READ_SUCCESS,
    playload
  };
}

export function repositoriesReadFailure(playload) {
  return {
    type: REPOSITORIES_READ_FAILURE,
    playload
  };
}

export function repositoriesThunkReadRequest(playload) {
  return {
    type: REPOSITORIES_THUNK_READ_REQUEST,
    playload
  };
}

export function repositoriesSagaReadRequest(playload) {
  return {
    type: REPOSITORIES_SAGA_READ_REQUEST,
    playload
  };
}
