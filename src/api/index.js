import axios from 'axios';
import dayjs from 'dayjs';

export const repositories = ({ language }) =>
  axios
    .get(
      `https://api.github.com/search/repositories?sort=stars&order=desc&q=language:${language}+created:>${dayjs()
        .subtract(1, 'month')
        .format('YYYY-MM-DD')}`
    )
    .then(res => res.data)
    .then(d => {
      const r = Math.floor(Math.random() * (7 - 1)) + 1;
      if (r > 5) {
        throw new Error(`error test: ${r}`);
      } else {
        return new Promise(res => {
          setTimeout(() => {
            res(d);
          }, r * 1000);
        });
      }
    });
