import React from 'react'
import produce from 'immer'

class ComClass extends React.Component {

  constructor(props) {
    super(props);
    this.state = { count: 0 }
    this.setState = this.setState.bind(this)
  }

  setCount(num) {
    this.setState(produce(preState => { preState.count += num }))
  }

  componentDidMount() {
    console.log('componentDidMount', this.state.count)
  }

  componentDidUpdate() {
    console.log('componentDidUpdate', this.state.count)
  }

  componentWillUnmount() {
    console.log('componentWillUnmount', this.state.count)
  }

  render() {
    const { count } = this.state
    return <div><button onClick={() => {
      this.setCount( 1)
    }}>add</button>&nbsp;&nbsp;ComClass:{count}</div>
  }
}

export default ComClass