import React, { useState, useEffect } from 'react';

function ComFunction() {
  const [count, setCount] = useState(0)

  useEffect(() => {
    console.log('useEffect-1', count)
    return () => {
      console.log('useEffect-2', count)
    }
  })

  return <div><button onClick={() => {
    setCount(count + 1)
  }}>add</button>&nbsp;&nbsp;ComFunction:{count}</div>
}

export default ComFunction
