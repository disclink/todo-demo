import React, { useState, useEffect } from 'react';

export default function Content() {
  
  const [content] = useState('pppppppppppp')

  useEffect(() => {
    console.log(`content 1 : ${content}`)
    return () => {
      console.log(`content 2 : ${content}`)
    }
  })

  return (
    <p>
      {content}
    </p>
  )
}