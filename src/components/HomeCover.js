import React from 'react';
import { Pane, Text } from 'evergreen-ui';

function HomeCover() {
  return (
    <Pane padding={12}>
      <img style={{width: '100%'}} src="/static/img/kiven-zhao-CVS4kWJaYLs-unsplash.jpg" alt="cover" />
      <Pane display="flex" justifyContent="flex-end" padding={8}>
      <Text size={300} color="muted">Photo by KiVEN Zhao on Unsplash</Text>
      </Pane>
    </Pane>
  );
}

export default HomeCover;
