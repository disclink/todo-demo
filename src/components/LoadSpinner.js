import React from 'react';
import { Pane, Spinner } from 'evergreen-ui'

function LoadSpinner() {
  return <Pane>
    <Spinner marginX="auto" marginY={80} />
  </Pane>
}

export default LoadSpinner;