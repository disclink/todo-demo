import React from 'react';
import { withRouter } from 'react-router-dom';
import { IconButton, Popover, Menu, Position } from 'evergreen-ui';

function MainMenu() {
  return (
    <Popover
      position={Position.BOTTOM_RIGHT}
      content={
        <Menu>
          <Menu.Group>
            <Menu.Item
              onSelect={() => {
                console.log('edit');
              }}
            >
              edit
            </Menu.Item>
          </Menu.Group>
        </Menu>
      }
    >
      <IconButton appearance="minimal" icon="menu" />
    </Popover>
  );
}

export default withRouter(MainMenu);
