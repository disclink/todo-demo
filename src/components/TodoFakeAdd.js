import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Pane, Button, IconButton, TextInput, toaster } from 'evergreen-ui';
import axios from 'axios';
import { todoBatchAdd, todosFilter } from '../actions/todos';

const MIN_SIZE = 1;
const MAX_SIZE = 10;

function TodoFakeAdd({ switchInputType }) {
  const [size, setSize] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  function handleChange(e) {
    const val = e.target.value;
    const num = Number(val);
    let s = num;
    if (isNaN(num)) {
      s = MIN_SIZE;
    } else if (num > MAX_SIZE) {
      s = MAX_SIZE;
    } else if (num < MIN_SIZE) {
      s = MIN_SIZE;
    }
    return setSize(s);
  }

  async function handleSumbit() {
    let fragments = [];
    let errorMessage = '';
    let count = 0;
    setIsLoading(true);
    let start = new Date() - 0;
    try {
      while (count < size) {
        let fragment = await axios
          .get('https://api.github.com/zen')
          .then(res => res.data);
        if (!!fragment) {
          fragments.push(fragment);
        }
        count++;
      }
    } catch (error) {
      errorMessage = error.message;
    }
    let end = new Date() - 0;
    let cost = (end - start) / 1000;
    toaster.notify(`It took ${cost.toFixed(2)}s to get ${size} fake TODO.`);
    if (!fragments || !Array.isArray(fragments) || fragments.length <= 0) {
      errorMessage = errorMessage || 'Unknown exception';
      toaster.warning(errorMessage);
    } else {
      dispatch(todoBatchAdd(fragments));
      dispatch(todosFilter(''));
    }
    setIsLoading(false);
  }

  function handleIncrease() {
    const s = size < MAX_SIZE ? size + 1 : MAX_SIZE;
    return setSize(s);
  }

  function handleDecrease() {
    const s = size > MIN_SIZE ? size - 1 : MIN_SIZE;
    return setSize(s);
  }

  return (
    <Pane
      paddingX={24}
      paddingY={12}
      display="flex"
      borderBottom="muted"
      background="#fff"
      alignItems="center"
      justifyContent="center"
    >
      <Pane display="flex" width={360} flexDirection="row">
        <IconButton
          icon="minus"
          onClick={handleDecrease}
          disabled={isLoading}
        />
        <TextInput
          width={64}
          placeholder="write something"
          value={size}
          disabled={isLoading}
          onChange={handleChange}
          onKeyPress={e => {
            if (e.key && e.key === 'Enter') {
              e.preventDefault();
              handleSumbit();
            }
          }}
        />
        <IconButton icon="plus" onClick={handleIncrease} disabled={isLoading} />
        <Pane flex={1} />
        <Button
          iconBefore="menu-open"
          onClick={handleSumbit}
          disabled={isLoading}
        >
          {isLoading ? 'loading...' : 'add fake'}
        </Button>
        <Pane width={12} />
        <IconButton
          icon="comparison"
          onClick={switchInputType}
          disabled={isLoading}
        />
      </Pane>
    </Pane>
  );
}

export default TodoFakeAdd;
