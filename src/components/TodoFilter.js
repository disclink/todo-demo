import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { Pane, SegmentedControl } from 'evergreen-ui';
import { todosFilterStatus, todosFilter } from '../actions/todos'

function TodoFilter() {

  const { COMPLETED, NOT_COMPLETED } = todosFilterStatus
  const todosVisibilityFilter = useSelector(state => state.todosVisibilityFilter)
  const dispatch = useDispatch()

  const options = [
    { label: 'all', value: '' },
    { label: 'completed', value: COMPLETED },
    { label: 'not completed', value: NOT_COMPLETED },
  ]

  return <Pane justifyContent="center" paddingX={24} paddingY={12} display="flex" borderBottom="muted" background="#fff">
    <SegmentedControl
      width={280}
      options={options}
      value={todosVisibilityFilter}
      onChange={value => {dispatch(todosFilter(value))}}
    />
  </Pane>
}

export default TodoFilter