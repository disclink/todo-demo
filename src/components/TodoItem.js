import React from 'react';
import {useDispatch} from 'react-redux'
import {Pane, Icon, Text} from 'evergreen-ui'
import {todoToggle, todoRemove} from '../actions/todos'


function TodoItem({ completed, text, id }) {

  const dispatch = useDispatch()

  function itemToggle(id) {
    if (!id) return
    dispatch(todoToggle(id))
    return 
  }

  function itemRemove(id) {
    if (!id) return
    dispatch(todoRemove(id))
    return
  }

  return <Pane paddingX={24} paddingY={12} marginBottom={12} background="tint1" display="flex">
    <Icon icon="tick-circle" color={!!completed ? 'success' : 'muted'} marginRight={20} marginTop={2} onClick={() => {itemToggle(id)}} />
    <Text flex={1}>{text}</Text>
    <Icon icon="cross" color="muted" marginLeft={20} marginTop={2} onClick={() => {itemRemove(id)}} />
  </Pane>
}

export default TodoItem