import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Pane, Spinner, Text } from 'evergreen-ui';
import InfiniteScroll from 'react-infinite-scroller';
import TodoItem from './TodoItem';
import { todosFilterStatus } from '../actions/todos';
import dayjs from 'dayjs';

function delayFunc(t = 1000) {
  return new Promise(res => {
    setTimeout(() => {
      res();
    }, t);
  });
}

function TodoList() {
  const { COMPLETED, NOT_COMPLETED } = todosFilterStatus;
  const todos = useSelector(state => state.todos);
  const todosVisibilityFilter = useSelector(
    state => state.todosVisibilityFilter
  );

  const [pageNum, setPageNum] = useState(0);

  const pageSize = 10;
  const delay = 500;

  function loadMore(p) {
    console.log(`loadMore: ${p}`);
    delayFunc(delay).then(() => {
      setPageNum(p);
    });
  }

  const flagPrefix = 'YYYY-MM-DD HH:00'
  let pointer = '';

  return (
    <Pane background="#fff">
      <InfiniteScroll
        pageStart={0}
        loadMore={loadMore}
        hasMore={todos.length > pageNum * pageSize}
        loader={<Spinner key={0} marginX="auto" marginY={12} />}
      >
        {todos
          .filter((item, index) => index < pageNum * pageSize)
          .filter(item => {
            if (todosVisibilityFilter === COMPLETED) {
              return item.completed;
            } else if (todosVisibilityFilter === NOT_COMPLETED) {
              return !item.completed;
            } else {
              return true;
            }
          })
          .map(item => {
            const d = dayjs(item.date);
            if (d.isValid()) {
              const p = d.format(flagPrefix);
              if (pointer !== p) {
                pointer = p;
                return [
                  <Pane
                    position="sticky"
                    borderBottom="default"
                    paddingX={24}
                    paddingY={4}
                    top={170}
                    background="tint2"
                    key={`${item.id}-${item.date}`}
                  >
                    <Text color="dark">{p}</Text>
                  </Pane>,
                  <TodoItem key={item.id} {...item} />
                ];
              }
            }
            return <TodoItem key={item.id} {...item} />;
          })}
      </InfiniteScroll>
      {todos.filter(item => {
        if (todosVisibilityFilter === COMPLETED) {
          return item.completed;
        } else if (todosVisibilityFilter === NOT_COMPLETED) {
          return !item.completed;
        } else {
          return true;
        }
      }).length > 0 ? (
        todos.length <= pageNum * pageSize && (
          <Pane
            display="flex"
            paddingY={8}
            justifyContent="center"
            background="tint2"
          >
            <Text>the end</Text>
          </Pane>
        )
      ) : (
        <Pane
          display="flex"
          paddingY={8}
          justifyContent="center"
          background="tint2"
        >
          <Text>no data</Text>
        </Pane>
      )}
    </Pane>
  );
}

export default TodoList;
