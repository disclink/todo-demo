import React from 'react';
import { Pane, Heading } from 'evergreen-ui';
import MainMenu from '../components/MainMenu';
import DocumentTitle from '../components/DocumentTitle';
import './Main.css';

function Main({ children, title = '' }) {
  return (
    <div className="main">
      <DocumentTitle title={title} />
      <Pane position="sticky" top={0} zIndex={10}>
        <Pane display="flex" paddingY={12} paddingX={24} background="tint2">
          <Pane display="flex" flex={1} alignItems="center">
            <Heading size={600}>{title}</Heading>
          </Pane>
          <Pane>
            <MainMenu />
          </Pane>
        </Pane>
      </Pane>
      <Pane>{children}</Pane>
    </div>
  );
}

export default Main;
