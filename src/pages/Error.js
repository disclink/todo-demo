import React, { useState, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Pane, Text, Link } from 'evergreen-ui';
import Main from '../layout/Main';
import ErrorCode from './ErrorCode';

let counter = null;

function Error(props) {
  const [count, setCount] = useState(5);

  useEffect(() => {
    if (count >= 0) {
      counter = setTimeout(() => {
        setCount(count - 1);
      }, 1000);
    }
    return () => {
      if (count < 0) {
        clearTimeout(counter);
      }
    };
  }, [count]);

  return (
    <Main title="Error">
      {count < 0 && (
        <Redirect to={{ pathname: '/', state: { from: props.location } }} />
      )}
      <Pane
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        paddingY={24}
      >
        <Text paddingY={8} size={600}>
          ERROR
        </Text>
        <Route path="/error/:code" component={ErrorCode} />
        <Text paddingY={8} size={300} color="muted">
          Return to the{' '}
          <Link
            href="#"
            onClick={() => {
              setCount(-1);
            }}
          >
            homepage
          </Link>{' '}
          after {count} seconds
        </Text>
      </Pane>
    </Main>
  );
}

export default Error;
