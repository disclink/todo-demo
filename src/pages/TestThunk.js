import React, { useEffect } from 'react';
import { Pane, Button, Table, Text, toaster } from 'evergreen-ui';
import { useSelector, useDispatch } from 'react-redux';
import Main from '../layout/Main';
import { repositoriesRead } from '../thunk/repositories';

function Test() {
  const { items = [], loading = false, error = '', language } = useSelector(
    state => state.repositories
  );
  const dispatch = useDispatch();

  useEffect(() => {
    if (!!error) {
      toaster.warning(error);
    }
  }, [error]);

  const languages = ['java', 'python', 'javascript'];

  return (
    <Main title="Test Thunk">
      <Pane display="flex" alignItems="center">
        <Pane flex={1} paddingX={20} paddingY={8}>
          <Pane paddingY={8}>
            {languages.map(lan => (
              <Button
                appearance="minimal"
                height={24}
                key={lan}
                color={lan === language ? 'danger' : 'selected'}
                onClick={() => {
                  dispatch(repositoriesRead(lan));
                }}
              >
                {lan}
              </Button>
            ))}
            {!!loading && (
              <Text paddingX={12} color="muted">
                loading...
              </Text>
            )}
          </Pane>
          {!!items && items.length > 0 && (
            <Pane>
              {' '}
              <Table>
                <Table.Head>
                  <Table.TextHeaderCell flexBasis={200}>
                    Repository
                  </Table.TextHeaderCell>
                  <Table.TextHeaderCell>Stars</Table.TextHeaderCell>
                </Table.Head>
                <Table.Body>
                  {items.map(item => (
                    <Table.Row key={item.id} isSelectable>
                      <Table.TextCell flexBasis={200}>
                        {item.name}
                      </Table.TextCell>
                      <Table.TextCell isNumber>
                        {item.stargazers_count}
                      </Table.TextCell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            </Pane>
          )}
        </Pane>
      </Pane>
    </Main>
  );
}

export default Test;
