import React, { useState } from 'react';
import Main from '../layout/Main';
import TodoList from '../components/TodoList';
import TodoFilter from '../components/TodoFilter';
import TodoInput from '../components/TodoInput';
import TodoFakeAdd from '../components/TodoFakeAdd';

function Todo() {
  const INPUT_TYPE = {
    FAKE: 'FAKE',
    MANUAL: 'MANUAL'
  };

  const [inputType, setInputType] = useState(INPUT_TYPE.MANUAL);

  function switchInputType() {
    const nextInputType =
      inputType === INPUT_TYPE.FAKE ? INPUT_TYPE.MANUAL : INPUT_TYPE.FAKE;
    setInputType(nextInputType);
  }

  return (
    <Main title="Todo">
      <div className="form-wrap" style={{ position: 'sticky', top: '56px' }}>
        <TodoFilter />
        {inputType === INPUT_TYPE.FAKE ? (
          <TodoFakeAdd {...{ switchInputType }} />
        ) : (
          <TodoInput {...{ switchInputType }} />
        )}
      </div>
      <TodoList />
    </Main>
  );
}

export default Todo;
