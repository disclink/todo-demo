import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { todos, todosVisibilityFilter } from './todos';
import { repositories } from './repositories';

const rootReducer = history =>
  combineReducers({
    router: connectRouter(history),
    todos,
    todosVisibilityFilter,
    repositories
  });

export default rootReducer;
