import {
  REPOSITORIES_READ_REQUEST,
  REPOSITORIES_READ_SUCCESS,
  REPOSITORIES_READ_FAILURE,
  REPOSITORIES_THUNK_READ_REQUEST,
  REPOSITORIES_SAGA_READ_REQUEST
} from '../actions/repositories';

// CREATE|UPDATE|READ|DELETE

const initState = {
  language: '',
  items: [],
  loading: false,
  error: ''
};

export function repositories(state = initState, action) {
  const { items = [], language = '', error = '' } = action.playload || {};
  switch (action.type) {
    case REPOSITORIES_READ_REQUEST:
    case REPOSITORIES_THUNK_READ_REQUEST:
    case REPOSITORIES_SAGA_READ_REQUEST:
      return { ...state, loading: true, error: '', language };
    case REPOSITORIES_READ_SUCCESS:
      return { ...state, loading: false, items, language };
    case REPOSITORIES_READ_FAILURE:
      return { ...state, loading: false, error };
    default:
      return state;
  }
}
