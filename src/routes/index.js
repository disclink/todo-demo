import React, { lazy } from 'react';
import { Redirect } from 'react-router-dom';

const homePage = [
  {
    path: '/',
    exact: true,
    component: lazy(() => import('../pages/TestThunk'))
  },
  {
    path: '/home',
    render: props => (
      <Redirect to={{ pathname: '/', state: { from: props.location } }} />
    )
  },
  {
    path: '/todo',
    render: props => (
      <Redirect to={{ pathname: '/', state: { from: props.location } }} />
    )
  }
];

const errorPage = [
  {
    component: lazy(() => import('../pages/NotFound'))
  }
];

const routes = [
  {
    path: '/test-thunk',
    component: lazy(() => import('../pages/TestThunk'))
  },
  {
    path: '/test-saga',
    component: lazy(() => import('../pages/TestSaga'))
  },
  {
    path: '/error',
    component: lazy(() => import('../pages/Error'))
  }
];

export default [...homePage, ...routes, ...errorPage];
