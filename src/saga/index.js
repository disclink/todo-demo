import { spawn } from 'redux-saga/effects';
import repositories from './repositories';

const makeRestartable = saga => {
  return function*() {
    yield spawn(function*() {
      while (true) {
        try {
          yield call(saga);
          console.error(
            'unexpected root saga termination. The root sagas are supposed to be sagas that live during the whole app lifetime!',
            saga
          );
        } catch (e) {
          console.error('Saga error, the saga will be restarted', e);
        }
        yield delay(1000);
      }
    });
  };
};

const rootSagas = [repositories].map(makeRestartable);

export default function* root() {
  yield rootSagas.map(saga => call(saga));
}
