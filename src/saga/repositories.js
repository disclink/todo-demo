import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'; // eslint-disable-line no-unused-vars
import * as API from '../api';
import {
  REPOSITORIES_SAGA_READ_REQUEST,
  REPOSITORIES_READ_SUCCESS,
  REPOSITORIES_READ_FAILURE
} from '../actions/repositories';

const ApiRepositoriesRead = lan =>
  API.repositories({ language: lan }).then(d => {
    const { items = [] } = d;
    return {
      items: items.map(({ id, name, stargazers_count }) =>
        Object.assign({}, { id, name, stargazers_count })
      ),
      language: lan
    };
  });

function* handleRepositoriesRead(action) {
  try {
    const playload = yield call(ApiRepositoriesRead, action.playload.language);
    yield put({ type: REPOSITORIES_READ_SUCCESS, playload });
  } catch (e) {
    yield put({
      type: REPOSITORIES_READ_FAILURE,
      playload: { error: e.message || 'no data' }
    });
  }
}

export default function* repositories() {
  yield takeLatest(REPOSITORIES_SAGA_READ_REQUEST, handleRepositoriesRead);
}
