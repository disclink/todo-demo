import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rootReducer from '../reducers';
import rootSaga from '../saga/repositories';

export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['todos']
};

const persistedReducer = persistReducer(persistConfig, rootReducer(history));

const middlewares = [routerMiddleware(history)];

const composeEnhancers =
  (process.env.NODE_ENV === 'development' &&
    window &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

const enhancer = composeEnhancers(
  applyMiddleware(...middlewares, thunk, sagaMiddleware)
);

const initialState = {};

const store = createStore(persistedReducer, initialState, enhancer);

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);

export default store;
