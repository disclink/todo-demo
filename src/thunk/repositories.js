import * as API from '../api';
import {
  repositoriesThunkReadRequest,
  repositoriesReadSuccess,
  repositoriesReadFailure
} from '../actions/repositories';

export function repositoriesRead(lan) {
  return function(dispatch) {
    dispatch(repositoriesThunkReadRequest());
    return API.repositories({ language: lan })
      .then(d => {
        const { items = [] } = d;
        return dispatch(
          repositoriesReadSuccess({
            items: items.map(({ id, name, stargazers_count }) =>
              Object.assign({}, { id, name, stargazers_count })
            ),
            language: lan
          })
        );
      })
      .catch(err =>
        dispatch(repositoriesReadFailure({ error: err.message || 'no data' }))
      );
  };
}
