import jsCookie from 'js-cookie';
import axios from 'axios';
import { toaster } from 'evergreen-ui';

export function getUserInfo() {
  const uid = jsCookie.remove('cur_uid');
  const name = jsCookie.remove('cur_name');
  const avatar_url = jsCookie.remove('cur_avatar_url');
  return {
    uid,
    name,
    avatar_url
  };
}

export function clearUserInfo() {
  jsCookie.remove('connect.sid');
  jsCookie.remove('cur_uid');
  jsCookie.remove('cur_name');
  jsCookie.remove('cur_avatar_url');
}

export function setUserInfo({ uid = '', name = '', avatar_url = '' }) {
  jsCookie.set('cur_uid', uid);
  jsCookie.set('cur_name', name);
  jsCookie.set('cur_avatar_url', avatar_url);
}

export async function fetchUserInfo() {
  const connectSid = jsCookie.get('connect.sid');
  if (!connectSid) {
    clearUserInfo();
  }
  let result = {};
  try {
    result = await axios.get('/api/user').then(r => r.data.playload);
  } catch (error) {
    toaster.warning(error.message || 'Unable to get user data');
  }
  const { uid = '', name = '', avatar_url = '' } = result || {};
  const userInfo = { uid, name, avatar_url };
  setUserInfo(userInfo);
  return userInfo;
}
